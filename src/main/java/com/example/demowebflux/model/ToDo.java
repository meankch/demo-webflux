package com.example.demowebflux.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class ToDo {
    @Id
    private String id;
    private String title;
    private boolean completed;
}