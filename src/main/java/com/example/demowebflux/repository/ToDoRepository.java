package com.example.demowebflux.repository;

import com.example.demowebflux.model.ToDo;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToDoRepository extends ReactiveCrudRepository<ToDo, String> {
}