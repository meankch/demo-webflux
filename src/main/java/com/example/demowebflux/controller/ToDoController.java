package com.example.demowebflux.controller;

import com.example.demowebflux.model.ToDo;
import com.example.demowebflux.repository.ToDoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;

@Slf4j
@RestController
@RequestMapping("/v1")
public class ToDoController {
    private ToDoRepository toDoRepository;

    public ToDoController(ToDoRepository toDoRepository) {
        this.toDoRepository = toDoRepository;
    }

    @GetMapping("/to-dos")
    public Flux<ToDo> allToDos() {
        log.info("Retrieving all ToDos");
        return toDoRepository.findAll();
    }

    @GetMapping("/to-dos/{id}")
    public Mono<ToDo> getToDo(@PathVariable String id) {
        log.info("Retrieve a to-do id=[{}]", id);
        return toDoRepository.findById(id);
    }

    @PostMapping("/to-dos")
    public Mono<ResponseEntity<?>> saveToDo(@RequestBody Mono<ToDo> ToDoMono) {
        return ToDoMono.flatMap(ToDo -> toDoRepository.save(ToDo).map(saved ->
                ResponseEntity.created(URI.create(String.format("/to-dos/%s", saved.getId()))).body(saved)));
    }

    @DeleteMapping("/to-dos/{id}")
    public Mono<Void> deleteToDo(@PathVariable String id) {
        log.info("Deleting a to-do id=[{}]", id);
        return toDoRepository.deleteById(id);
    }
}